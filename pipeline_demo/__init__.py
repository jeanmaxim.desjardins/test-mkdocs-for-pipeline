"""A demo pacakge to show how to use MkDocs with python packages"""

__version__ = "0.1.0"

from .greeter import Greeter, PoliteGreeter

# Public classes
__all__ = ["Greeter", "PoliteGreeter"]
