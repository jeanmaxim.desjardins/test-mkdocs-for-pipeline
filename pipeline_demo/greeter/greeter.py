"""
This module provides classes for greeting people.

It's pretty boring and simple, but here we are.
"""

from typing import Annotated, Literal
from typing_extensions import Doc


class Greeter:
    """
    A class that greets the world. Here is the docstring directly in the `Greeter` class.

    For our demonstration purposes, we will use this class to showcase the different ways to document
    a class and its methods. We will use the `Annotated` style for the type hints and the `Doc` style.
    """

    def __init__(
        self,
        name: Annotated[
            str,
            Doc(
                """
            `Annotated` style: The name of the person to greet. I strongly recommend this be "John", he needs some love.
            Notice this block is automatically added to the docstring of the `name` parameter and will
            be displayed in the documentation. But please be careful has `Annotaded` requires python 3.9
            and up.

            Note:
                - This doc will not show up without using the `griffe_typingdoc` extension.
            """
            ),
        ],
        age: Annotated[
            int,
            Doc(
                """
                `Annotated` style: The age of the person to greet. I strongly recommend this be 42,
                it's the answer to everything.
                This `Annotated` docstring will be displayed in the documentation. Once again, be careful with
                the python version.
                """
            ),
        ] = None,
    ) -> None:
        """
        Initialize the greeter. Here is the docstring for the `__init__` method.

        Args:
            name (str): The name of the person to greet. and will show in the documentation.
            age (int, optional): The age of the person to greet. Defaults to None and will show automatically
                in the documentation as well.

        Returns:
            None
        """
        self.name = name

    def greet(
        self,
    ) -> Annotated[
        int,
        Doc(
            """The greeting message personnalized for the person to greet.
            This docstring from the `Annotaded` return type hint will be displayed in the documentation
            if we inforce this style, use python 3.9+, and have the `griffe_typingdoc` extension. for mkdocs"""
        ),
    ]:
        """
        Greet a rando by saying hello.

        Returns:
            A string with the greeting. This docstring is super classic and will be displayed in the documentation,
                not the `Annotated` one you can see in the source code.
        """
        return f"Hello, {self.name}!"

    def greet_a_number_of_times(self, number: int) -> str:
        """
        Greet a rando by saying hello `n` times. Please refer to the
        [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html)
        for more information on how to write docstrings. In python 3, type hints are used to
        specify the type of the parameters and the return value,
        so we don't add them in the docstring as shown in the source code.


        Args:
            number: The number of times to say hello. Don't add a type to this parameter, we want to
        Returns:
            A string with the greeting repeated n times.
        """
        return f"Hello, {self.name}! " * number

    def legacy_greet(self, message):
        """
        Greet a rando by saying hello with a message. This is a legacy method without docstrings.

        `griffe` will usually display a warning when it encounters a method without a type
        annotation.

        Args:
            message (str): The message to greet our person.

        Returns:
            str: A string with the greeting and the message.
        """
        return f"Hello, {self.name}. {message}"


class PoliteGreeter(Greeter):
    """
    A class that greets a rando politely. This class inherits from the `Greeter` class, mkdocs will display this.
    """

    def greet(self) -> str:
        """
        Greet a rando by saying hello politely.
        """
        return f"Hello, {self.name}. How are you today?"

    def _private_method(self, param1: int, param2: str) -> None:
        """
        A private method that does nothing. This method will not show up in the documentation.

        Args:
            param1: An integer.
            param2: A string.
        """
        pass

    def overloaded_greetings(
        self,
        title: Literal["Mr.", "Mrs.", "Dr."] = "Mrs.",
        is_aristocrat: bool = False,
        greet_message: str = "Buongiorno",
        name: str = "John",
    ) -> str:
        """
        A stupid overloaded method to show a more complex method signature and its documatation.

        Args:
            title: Our person's title. Default to "Mrs."
            is_aristocrat: A boolean to know if our person is an aristocrat. Default to False.
            greet_message: The message to greet our person. Default to "Buongiorno", don't you dare change it...
            name: The name of the person to greet. Default to "John" because we established that he needs some love.

        Returns:
            A string with the personalized greeting message.
        """
        if is_aristocrat:
            return f"{greet_message}, {name}, {title} money bags."
        return f"{greet_message}, {title} {name}."
