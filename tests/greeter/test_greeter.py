"""
Stupid simple tests for the Greeter class.
Will use there to demonstrate coverage in CI.
"""

from pipeline_demo import Greeter, PoliteGreeter


def test_greeter_init():
    greeter = Greeter("John", 42)
    assert greeter.name == "John"


def test_greeter_greet():
    greeter = Greeter("John", 42)
    assert greeter.greet() == "Hello, John!"


def test_greeter_greet_a_number_of_times():
    greeter = Greeter("John", 42)
    assert (
        greeter.greet_a_number_of_times(3) == "Hello, John! Hello, John! Hello, John! "
    )


def test_greeter_legacy_greet():
    greeter = Greeter("John", 42)
    assert greeter.legacy_greet("How are you?") == "Hello, John. How are you?"


def test_polite_greeter_greet():
    polite_greeter = PoliteGreeter("John", 42)
    assert polite_greeter.greet() == "Hello, John. How are you today?"


def test_polite_greeter_overloaded_greetings():
    polite_greeter = PoliteGreeter("John", 42)
    assert (
        polite_greeter.overloaded_greetings("Dr.", True, "Good day", "Doe")
        == "Good day, Doe, Dr. money bags."
    )
    assert (
        polite_greeter.overloaded_greetings("Mr.", False, "Hello", "Doe")
        == "Hello, Mr. Doe."
    )
