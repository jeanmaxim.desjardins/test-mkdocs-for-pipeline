This site contains the project documentation for the
`pipeline_demo` project that is just that, a demo to
show how we can use MkDocs:

[Build Your Python Project Documentation With MkDocs](
    https://realpython.com/python-project-documentation-with-mkdocs/).

This project aims is to give a framework and starting point to add documentation
to your projects

## Table Of Contents

1. [Quick Start](quick_start.md)
2. [Reference](reference/index.md)

The [Quick Start](quick_start.md) section will get you up and running to play around with the project.
The [Reference](reference/index.md) section shows what documenting some python modules with `mkdocstrings`
could work with a few very simple examples.

## Acknowledgements

I would like to thank my cats and my mom, and Audrey I guess