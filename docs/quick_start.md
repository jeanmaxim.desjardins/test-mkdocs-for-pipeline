Welcome to the **Pipeline MkDocs Demo** tutorial! 📝

This tutorial's goal is to get you up and ready with MkDocs in an existing Python project and how to use it wit Gitlab pages.

> **Note**: Those instructions are very high level, we assume you are familiar with python developement.

**Gitlab CI config**
Installing the virtual environment, running tests and building the docs to deploy on gitlab is accomplished with the simple
`.gitlab-ci.yml`

```yaml
image: python:latest

before_script:
  - pip install poetry
  - poetry config virtualenvs.create false
  - poetry install --no-interaction --no-ansi

test:
  stage: test
  script:
  - mkdocs build --verbose --site-dir docs_dev
  artifacts:
    paths:
    - docs_dev
  only:
  - dev

pages:
  stage: deploy
  script:
  - mkdocs build --verbose --site-dir public
  artifacts:
    paths:
    - public
  only:
  - main
```


**Local Developement**
All package management is done with [Poetry](https://python-poetry.org/).

Clone the repo
```sh
git clone https://gitlab.com/jeanmaxim.desjardins/test-mkdocs-for-pipeline.git
<or>
git clone git@gitlab.com:jeanmaxim.desjardins/test-mkdocs-for-pipeline.git

cd test-mkdocs-for-pipeline
```
Install poetry and the project dependencies

```sh
pip install poetry
poetry install
```

Adn finally, activate the environment
```sh
poetry shell
```

The `mkdocs.yml` is at the root of this project, so build and serve for local dev with:
```sh
mkdocs build && mkdocs serve
```

The docs will be available locally at: [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

**Congratulations! 🎉** You're all set to start using `MkDocs` in your own Python projects.