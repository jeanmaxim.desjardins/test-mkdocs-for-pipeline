# `Greeter` class
Here's the reference for the `greeter` module, it's classes with its parameters, attributes and methods

`Greeter` or `PoliteGreeter` can be imported from `pipeline_demo.greeter` module

```python
from pipeline_demo import Greeter, PoliteGreeter
```

Or by importing the whole `greeter` module from the `pipeline_demo` package.

```python
from pipeline_demo import greeter
```

> **Note** By importing the full directly with the special string:
> `::: pipeline_demo.greeter.greeter`
> we ensure that this module's doctstrings are included, and not just Classes and functions docstrings.

::: pipeline_demo.greeter.greeter
