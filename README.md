# Pipeline Demo for MkDocs

This project is a simple demo python project with a fully configured `MkDocs` Gitlab CI workflow.
The docs use modern features like search, the material design theme, code highlighting and more.

The python documentation is automatically built with `mkdocstrings`.

See the docs [here](https://test-mkdocs-for-pipeline-jeanmaxim-desjardins-89a971ceb9db5573c.gitlab.io/)
